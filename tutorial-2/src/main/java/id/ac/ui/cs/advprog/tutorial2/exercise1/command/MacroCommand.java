package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Stream;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        Stream<Command> stream = commands.stream();
        stream.forEach(command -> command.execute());
    }

    @Override
    public void undo() {
        ListIterator<Command> iter = commands.listIterator(commands.size());
        while (iter.hasPrevious()) {
            iter.previous().undo();
        }
    }
}
