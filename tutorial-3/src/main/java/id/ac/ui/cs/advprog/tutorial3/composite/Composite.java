package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class Composite {

    public static void main(String[] args) {
        Company company = new Company();

        Employees ceo = new Ceo("Supri", 10000000);
        Employees cto = new Cto("Julaeha", 5000000);
        Employees designer = new UiUxDesigner("Kiki", 300000);

        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(designer);

        System.out.println(company.getNetSalaries());
    }
}