package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    //TODO Implement

    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary >= 90000) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
        this.role = "UI/UX Designer";
    }

    public double getSalary() {
        return this.salary;
    }
}
