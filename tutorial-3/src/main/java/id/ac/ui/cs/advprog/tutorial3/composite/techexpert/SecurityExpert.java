package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement

    public SecurityExpert(String name, double salary) {
        this.name = name;
        if (salary >= 70000) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
        this.role = "Security Expert";
    }

    public double getSalary() {
        return this.salary;
    }
}
