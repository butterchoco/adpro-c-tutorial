package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        //TODO Implement
        this.food = food;
        this.food.setDescription("tomato sauce");
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return food.getDescription();
    }

    @Override
    public double cost() {
        //TODO Implement
        return 0.20 + food.cost();
    }
}
