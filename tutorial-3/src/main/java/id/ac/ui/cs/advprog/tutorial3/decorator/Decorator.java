package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;

public class Decorator {
    public static void main(String[] args) {
        Food thickBunBurger = new ThickBunBurger();
        Food hamburger = new BeefMeat(thickBunBurger);
        Food cheeseBurger = new Cheese(thickBunBurger);

        System.out.printf("Food : %s (%f)", cheeseBurger.getDescription(), cheeseBurger.cost());

    }
}