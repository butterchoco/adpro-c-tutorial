package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    
    public void fly() {
        System.out.println("It can not fly !");
    }
}
